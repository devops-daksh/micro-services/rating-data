package com.daksh.ratingdata.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daksh.ratingdata.UserRating;
import com.daksh.ratingdata.model.Rating;

@RestController
@RequestMapping("/ratings")
public class RatingController {

	@RequestMapping("/{movieId}")
	public Rating getRating(@PathVariable("movieId") String movieId) {
		return new Rating(movieId, 5);
	}
	
	@RequestMapping("/users/{userId}")
	public UserRating getUsersRating(@PathVariable("userId") String userId) {
		List<Rating> ratings = Arrays.asList(
					new Rating("550", 5), 
					new Rating("19404", 5)
				);
		UserRating userRating =  new UserRating();
		userRating.setUserId(userId);
		userRating.setRatings(ratings);
		
		return userRating;
	}
}
